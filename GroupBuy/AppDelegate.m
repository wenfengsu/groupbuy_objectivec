//
//  AppDelegate.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-06.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "GBRootViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize userDefault;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //return YES;
    
    
    UIViewController *initialViewController = nil;
    userDefault = [NSUserDefaults standardUserDefaults];
    
    FBSDKAccessToken *fbAccessToken = [FBSDKAccessToken currentAccessToken];
    
    
    //NSLog(@"fb access token: %@", fbAccessToken.tokenString);
    
      
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"id"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user in delegate:%@", result);
             }
         }];
    }
    
    if ([userDefault stringForKey:@"gbAccessToken"]) {
        
        NSLog(@"%@, %@", [userDefault stringForKey:@"gbAccessToken"], fbAccessToken);
        
        NSLog(@"alaredy login");
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"root" bundle:nil];
        initialViewController = [mainStoryBoard instantiateInitialViewController];

    }else{
        
        //not login
        UIStoryboard *loginOptionsStoryBoard = [UIStoryboard storyboardWithName:@"login" bundle:nil];
        initialViewController = [loginOptionsStoryBoard instantiateInitialViewController];

    }
    // if user is not logged in, we use loginOptionsViewController
    
    
    

    
    // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    //NSLog(@"init: %@", initialViewController);
    // Set the initial view controller to be the root view controller of the window object
    self.window.rootViewController  = initialViewController;
    
    
    // Set the window object to be the key window and show it
    [self.window makeKeyAndVisible];
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)swapRootView{
    
    
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"should push to another view with logged in user %@", userData);
    /*NSString *email = [userData stringForKey:@"email"];
    NSString *socialId = [userData stringForKey:@"id"];
    NSString *name = [userData stringForKey:@"name"];
    NSString *avatar = [userData stringForKey:@"picture.data.url"];
    NSString *gbAccessToken = [userData stringForKey:@"gbAccessToken"];*/
    
    NSLog(@"userData after login: %@", userData);
    
    UIViewController *rootViewController = nil;
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"root" bundle:nil];
    rootViewController = [mainStoryBoard instantiateInitialViewController];
    //GBRootViewController *rootViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"RootViewController"];
    
    //GBRootViewController *rootViewController = [GBRootViewController new];
    self.window.rootViewController  = rootViewController;
    [self.window makeKeyAndVisible];
    
    
    
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

@end
