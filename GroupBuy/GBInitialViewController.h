//
//  GBLoginOptionsViewController.h
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-18.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBAccountManager.h"
#import "AppDelegate.h"

@interface GBInitialViewController : UIViewController <GBAccountLoginDelegate>{
    GBAccountManager *account;
    AppDelegate *appDelegate;
    
}

@property (nonatomic) IBOutlet UIButton *facebookButton;
@property (nonatomic) IBOutlet UIButton *googleButton;
@property (nonatomic) IBOutlet UIButton *twitterButton;

@property (nonatomic) IBOutlet UIButton *loginButton;
@property (nonatomic) IBOutlet UIButton *registerButton;

@property (nonatomic) IBOutlet UITextField *emailTextField;
@property (nonatomic) IBOutlet UITextField *passwordTextField;


@end
