//
//  GBSUIImageView.m
//  GroupBuy
//
//  Created by Edmund Su on 2015-11-11.
//  Copyright © 2015 Wenfeng Su. All rights reserved.
//

#import "UIImageView+GBGroupBuy.h"

//#define IMG_DOMAIN @"http://www.bestbuy.ca"
#import <objc/runtime.h>

@interface UIImageView (_GBGroupBuy)

@property (readwrite, nonatomic, strong) NSBlockOperation *imageRequestOperation;

@end


@implementation UIImageView (_GBGroupBuy)

+ (NSOperationQueue *)imageRequestOperationQueue {
    static NSOperationQueue *_imageRequestOperationQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _imageRequestOperationQueue = [[NSOperationQueue alloc] init];
        _imageRequestOperationQueue.maxConcurrentOperationCount = NSOperationQueueDefaultMaxConcurrentOperationCount;
    });
    
    return _imageRequestOperationQueue;
}

- (NSOperation *)imageRequestOperation{
    return (NSOperation *)objc_getAssociatedObject(self, @selector(imageRequestOperation));
}

-(void)setImageRequestOperation:(NSOperation *)imageRequestOperation{
    objc_setAssociatedObject(self, @selector(imageRequestOperation), imageRequestOperation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end





@implementation UIImageView (GBGroupBuy)


@dynamic sessionManager, downloadRequest;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)setImageWithURL:(NSURL *)url {
    
    [self setImageWithURL:url placeholderImage:nil];
    
}

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholderImage
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self setImageWithURL:url placeholderImage:placeholderImage success:nil failure:nil];
}

- (void)setImageWithURL:(NSURL *)url
              placeholderImage:(UIImage *)placeholderImage
                       success:(void (^)(NSURLRequest *request, NSHTTPURLResponse * __nullable response, UIImage *image))success
                       failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse * __nullable response, NSError *error))failure
{
    
    if (placeholderImage) {
        self.image = placeholderImage;
    }
    
    __weak typeof (self) weakSelf = self;
    void (^imageRequest)() = ^{
        
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        

        __strong typeof (weakSelf) strongSelf = weakSelf;
        dispatch_sync(dispatch_get_main_queue(), ^{
            strongSelf.image = [UIImage imageWithData:imageData];
            strongSelf.image = self.image;
            
        });
    };
    
    [[UIImageView imageRequestOperationQueue] addOperationWithBlock:imageRequest];
    
    
    
}


- (void)cancelImageRequestOperation {
    
}


/*
-(void)setImageWithURL:(NSString *)imgUrl{
    
    
    if (self.downloadRequest)
    {
        [self.downloadRequest cancel];
    }
    
    
    //NSString *urlString = [NSString stringWithFormat:@"%@%@", imgUrl, imgUrl];
    NSString *urlString = imgUrl;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filename = [[imgUrl componentsSeparatedByString:@"/"] lastObject];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectoryPath = [paths objectAtIndex:0];
    
    
    NSURL *destinationURL = [NSURL fileURLWithPath:[documentDirectoryPath stringByAppendingPathComponent:filename]];
    if (![fileManager fileExistsAtPath:[destinationURL path]])
    {
        self.sessionManager = [GBSSessionManager sharedSessionManager];
        
        
        __weak typeof (self) weakSelf = self;
        self.downloadRequest = [self.sessionManager downloadTaskWithUrl:urlString success:^(NSURLSessionDownloadTask *task, id responseObject) {
            
            NSURL *tempUrl = (NSURL *)responseObject;
            NSError *error;
            [fileManager moveItemAtURL:tempUrl toURL:destinationURL error:&error];
            
            __strong typeof (weakSelf) strongSelf = weakSelf;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                UIImage *image = [UIImage imageWithContentsOfFile:[destinationURL path]];
                
                [strongSelf setImage:image];
                
            });
            
        } failure:^(NSURLSessionDownloadTask *task, NSError *error) {
            NSLog(@"soemthing wrong: %@", error);
        }];
        
        
        
    }else{
        [self setImage:[UIImage imageWithContentsOfFile:[destinationURL path]]];
        
    }
    
    
}

-(void)cancelImageDownload{
    if ([self.downloadRequest state] == NSURLSessionTaskStateRunning) {
        [self.downloadRequest cancel];
    }
    
}*/


@end
