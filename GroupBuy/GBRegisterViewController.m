//
//  GBRegisterViewController.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-19.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import "GBRegisterViewController.h"

@interface GBRegisterViewController ()

@end

@implementation GBRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)removeModel:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

@end
