//
//  GBLoginOptionsViewController.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-18.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import "GBInitialViewController.h"
#import "GBLoginViewController.h"
#import "GBRegisterViewController.h"


@interface GBInitialViewController ()

@end

@implementation GBInitialViewController

@synthesize facebookButton, googleButton, twitterButton, loginButton, registerButton, emailTextField, passwordTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    account = [GBAccountManager new];
    account.accountLogindelegate = self;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    //[[GBSDataRequest sharedRequest] GET:@"test" parameters:nil success:nil failure:nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //NSLog(@"%@", [segue identifier]);
    // Make sure your segue name in storyboard is the same as this line
    
    if ([[segue identifier] isEqualToString:@"Register"])
    {
        // Get reference to the destination view controller
        //GBRegisterViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        //[vc setMyObjectHere:object];
    }else{
        
    }
}

- (IBAction)facebookConnectButtonCliked:(id)sender{
    NSLog(@"hello facebook");
    [account facebookLoginClicked];
    
    //[appDelegate swapRootView];
}

- (IBAction)googleConnectButtonCliked:(id)sender{
    NSLog(@"hello google");
}


-(void) registerButtonClicked{
    
}


-(void) loginButtonClicked{
    
}

#pragma mark -Account manager delegate
- (void) accountManager:(id)manager didSuccessfullyLogin:(id)options{
    NSLog(@"did login");
    //[account signupAndLoginUserWithGroupBuy];
    [appDelegate swapRootView];
}

- (void) accountManager:(id)manager didFailedLogin:(id)options{
    NSLog(@"failed");
}

@end
