//
//  GBAccountManager.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-06.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import "GBAccountManager.h"
#import "GBSSessionManager.h"

@implementation GBAccountManager

@synthesize accountLogindelegate;



- (id)initWithAcountType: (GBAccountType *) type {
    if (self = [super init]) {
        accountType = type;
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}



- (void) facebookLoginClicked{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in: %@", result);
             //[self updateContent];
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"picture, email, name"}]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                  if (!error) {
                      NSLog(@"fetched user11111:%@", result);
                      NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
                      [userData setObject:result[@"email"] forKey:@"email"];
                      [userData setObject:result[@"id"] forKey:@"id"];
                      [userData setObject:result[@"name"] forKey:@"name"];
                      [userData setObject:result[@"picture"] forKey:@"picture"];
                      [userData synchronize];
                      
                      [self facebookLoginSuccessed];
                      
                      
                  }else{
                      [self.accountLogindelegate accountManager:self didFailedLogin:nil];
                  }
              }];
             
         }
     }];
}

- (void)signupAndLoginUserWithGroupBuy{

}


- (void)facebookLoginSuccessed{
    
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"should push to another view with logged in user %@", userData);
    NSString *email = [userData stringForKey:@"email"];
    NSString *socialId = [userData stringForKey:@"id"];
    NSString *name = [userData stringForKey:@"name"];
    NSString *loginType = @"FB";
    FBSDKAccessToken *token = [FBSDKAccessToken currentAccessToken];
    
    NSString *url = @"http://198.58.109.220/api/v1.0/users/";
    NSDictionary *parameters = @{@"email": email, @"username": name, @"social_id": socialId, @"login_type": loginType, @"fb_access_token": token.tokenString};
    
    [[GBSSessionManager sharedSessionManager] POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"got result: %@", responseObject);
        
        NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
        
        NSString *gbAccessToken = [responseObject objectForKey:@"token"];
        [userData setObject:gbAccessToken forKey:@"gbAccessToken"];
        [userData synchronize];
        
        
        __weak typeof (self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            
                [weakSelf.accountLogindelegate accountManager:self didSuccessfullyLogin:responseObject];
        });
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error account manager: %@", error);
        
        __weak typeof (self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.accountLogindelegate accountManager:self didFailedLogin:error];
            //[weakSelf.accountLogindelegate accountManager:self didSuccessfullyLogin:responseObject];
        });
    }];
    
    
    
    
    /*GBHttpManager *httpManager = [GBHttpManager httpManager];
    httpManager.delegate = self;
    [httpManager getData:@"users" requestType:GBRequestTypePost parameters:parameters];*/
    
}

/*
#pragma mark - GBHttpRequestManagerDelegate
-(void) httpManager:(id)manager didFinishLoadingWithResult:(NSDictionary*) results{
    
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    
    NSString *gbAccessToken = [results objectForKey:@"token"];
    [userData setObject:gbAccessToken forKey:@"gbAccessToken"];
    [userData synchronize];
    
    //NSLog(@"JSON: %@", results);
    //[self.accountLogindelegate accountManager:self didSuccessfullyLogin:results];
}*/

/*
-(void) httpManager:(id)manager didFinishWithError:(NSDictionary *) results{
    [self.accountLogindelegate accountManager:self didFailedLogin:results];
}*/



@end
