//
//  GBSUIImageView.h
//  GroupBuy
//
//  Created by Edmund Su on 2015-11-11.
//  Copyright © 2015 Wenfeng Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GBSSessionManager.h"

@interface UIImageView (GBGroupBuy)

@property (nonatomic, strong) NSURLSessionDownloadTask *downloadRequest;
//@property (nonatomic, strong) NSURLSessionDataTask *imageDownloadTask;
@property (nonatomic, strong) GBSSessionManager *sessionManager;

-(void)setImageWithURL:(NSURL *)imgUrl;


//-(void)cancelImageDownload;

@end
