//
//  GBRootViewController.h
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-18.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GBRootViewController : UIViewController{
    NSMutableArray *listings;
    NSString *nextURL;
}


@property(nonatomic) IBOutlet UITableView *listingTableView;
@property(nonatomic) IBOutlet UIButton *buyingButton;
@property(nonatomic) IBOutlet UIButton *sellingButton;

@property(nonatomic) NSMutableArray *listings;
@property(nonatomic, copy) NSString *nextURL;



@end
