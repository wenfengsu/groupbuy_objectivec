//
//  GBLoginViewController.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-22.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import "GBLoginViewController.h"


@interface GBLoginViewController ()

@end

@implementation GBLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    account = [GBAccountManager new];
    account.accountLogindelegate = self;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
- (IBAction)facebookLoginButtonCliked:(id)sender{
    [account facebookLoginClicked];
    
}

- (IBAction)removeModelAAA:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}*/

/*
#pragma mark -Account manager delegate
- (void) accountManager:(id)manager didSuccessfullyLogin:(id)options{
    NSLog(@"did login");
    [account signupAndLoginUserWithGroupBuy];
}

- (void) accountManager:(id)manager didFailedLogin:(id)options{
    NSLog(@"failed");
}*/



@end
