//
//  GBSSessionManager.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-11-07.
//  Copyright © 2015 Wenfeng Su. All rights reserved.
//

#import "GBSSessionManager.h"

@interface GBSSessionManager()

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSessionConfiguration *congiuration;


@end

@implementation GBSSessionManager

static GBSSessionManager *sessionManager = nil;
static NSURLSession *session = nil;

+(instancetype) sharedSessionManager{
    @synchronized(self) {
        if (sessionManager == nil) {
            sessionManager = [self new];
        }
    }
    
    return sessionManager;
}

+ (id)new
{
    return [[self alloc] init];
}

- (id)init{
    self = [super init];
    _session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:nil];
    return self;
}


/**
 This creates a new query parameters string from the given NSDictionary.
 @return The created parameters string.
 */
static NSString* NSStringFromQueryParameters(NSDictionary* queryParameters)
{
    NSMutableArray* parts = [NSMutableArray array];
    [queryParameters enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
        NSString *part = [NSString stringWithFormat: @"%@=%@",
                          [key stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]],
                          [value stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]
                          ];
        [parts addObject:part];
    }];
    
    return [parts componentsJoinedByString: @"&"];
}

/**
 Creates a new URL by adding the given query parameters.
 @param URL The input URL.
 @param queryParameters The query parameter dictionary to add.
 @return A new NSURL.
 */
/*
static NSURL* NSURLByAppendingQueryParameters(NSURL* URL, NSDictionary* queryParameters)
{
    NSString* URLString = [NSString stringWithFormat:@"%@?%@",
                           [URL absoluteString],
                           NSStringFromQueryParameters(queryParameters)
                           ];
    return [NSURL URLWithString:URLString];
}*/

- (NSURLRequest *)requestWithMethod:(NSString *)method urlString:(NSString *)urlString parameters:(NSDictionary *)parameters{
    
   
    NSURL * url = [NSURL URLWithString:urlString];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    urlRequest.HTTPMethod = method;
    if ([method isEqualToString:@"POST"]) {
        NSError *error = nil;
        urlRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];

    }else{
        NSString *params = NSStringFromQueryParameters(parameters);
        [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    return urlRequest;
}


- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    
    NSURLSessionDataTask *dataTask = [self dataTaskWithHTTPMethod:@"GET" URLString:URLString parameters:parameters success:success failure:failure];
    
    [dataTask resume];
    return dataTask;
    
}


- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    
    NSURLSessionDataTask *dataTask = [self dataTaskWithHTTPMethod:@"POST" URLString:URLString parameters:parameters success:success failure:failure];
    
    [dataTask resume];
    
    return dataTask;
}

- (NSURLSessionDataTask *)dataTaskWithHTTPMethod:(NSString *)method
                                       URLString:(NSString *)URLString
                                      parameters:(id)parameters
                                         success:(void (^)(NSURLSessionDataTask *, id))success
                                         failure:(void (^)(NSURLSessionDataTask *, NSError *))failure
{
    
    
    /*NSURL *url = [NSURL URLWithString:URLString];
    
    url = NSURLByAppendingQueryParameters(url, parameters);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];*/
    
    NSURLRequest *request = [self requestWithMethod:method urlString:URLString parameters:parameters];
    
    
    
    __block NSURLSessionDataTask *dataTask = nil;
    
    dataTask = [_session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        
        
        if (error) {
            if (failure) {
                failure(dataTask, error);
            }
        }else{
            NSLog(@"success1111");
            if (success) {
                NSLog(@"success222222");
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                if (error) {
                    NSLog(@"success33333");
                    if (failure) {
                        failure(dataTask, error);
                    }
                }else{
                    NSLog(@"success444444");
                    success(dataTask, result);
                }
                
            }
        }
    }];
    
    
    return dataTask;
}

- (NSURLSessionDownloadTask *)downloadTaskWithUrl:(NSString *)URLString
                                          success:(void (^)(NSURLSessionDownloadTask *task, id responseObject))success
                                          failure:(void (^)(NSURLSessionDownloadTask *task, NSError *error))failure{
    
    
    NSURL *url = [NSURL URLWithString:URLString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    __block NSURLSessionDownloadTask *downloadTask = [_session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            if (failure) {
                failure(downloadTask, error);
            }
        }else{
            if (success) {
                success(downloadTask, location);
            }
        }
        
    }];
    
    [downloadTask resume];
    
    return downloadTask;
    
}

#pragma mark - NSURLSessionDownloadDelegate
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    
    NSLog(@"didFinishDownloadingToURL");
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
    // download task or data task will hit here whenever the task is done
    NSLog(@"didCompleteWithError: %@", error);
    
}

-(void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    
}

#pragma mark - NSURLSessionDelegate

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session {
    
}

#pragma mark - NSURLSessionDataDelegate
- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    
    completionHandler(NSURLSessionResponseAllow);
    NSLog(@"session data delegate didReceiveResponse");
    
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didBecomeDownloadTask:(NSURLSessionDownloadTask *)downloadTask
{
    NSLog(@"session data delegate didBecomeDownloadTask");
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    //[self.mutableData appendData:data];
    NSLog(@"session data delegate didReceiveData");
    
}


- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
 willCacheResponse:(NSCachedURLResponse *)proposedResponse
 completionHandler:(void (^)(NSCachedURLResponse *cachedResponse))completionHandler
{
    NSLog(@"session data delegate willCacheResponse");
    
}



@end
