//
//  GBLoginViewController.h
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-22.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBAccountManager.h"
#import "AppDelegate.h"

@interface GBLoginViewController : UIViewController{
    
    
    GBAccountManager *account;
    AppDelegate *appDelegate;
}

@end
