//
//  AppDelegate.h
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-06.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSUserDefaults *userDefault;



-(void)swapRootView;
@end

