//
//  GBSListDetailViewController.h
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-10-14.
//  Copyright © 2015 Wenfeng Su. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+GBGroupBuy.h"

@interface GBSListDetailViewController : UIViewController
    

@property(nonatomic) IBOutlet UITextView *textView;
@property (nonatomic, strong) IBOutlet UIImageView *itemImageView;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) IBOutlet UIView *tagsView;
@property(nonatomic, strong) NSDictionary *itemDetail;

 
 @end
