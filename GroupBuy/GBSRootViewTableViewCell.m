//
//  GBSRootViewTableViewCell.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-10-11.
//  Copyright © 2015 Wenfeng Su. All rights reserved.
//

#import "GBSRootViewTableViewCell.h"

@implementation GBSRootViewTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
