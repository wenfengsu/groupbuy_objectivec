//
//  GBSSessionManager.h
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-11-07.
//  Copyright © 2015 Wenfeng Su. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBSSessionManager : NSObject  <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate, NSURLSessionDownloadDelegate>


+(instancetype) sharedSessionManager;


- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionDownloadTask *)downloadTaskWithUrl:(NSString *)URLString
                                          success:(void (^)(NSURLSessionDownloadTask *task, id responseObject))success
                                          failure:(void (^)(NSURLSessionDownloadTask *task, NSError *error))failure;



@end
