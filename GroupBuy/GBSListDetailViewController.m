//
//  GBSListDetailViewController.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-10-14.
//  Copyright © 2015 Wenfeng Su. All rights reserved.
//

#import "GBSListDetailViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface GBSListDetailViewController ()

@end

@implementation GBSListDetailViewController

@synthesize textView, itemDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    textView.layer.cornerRadius = 3.0f;
    textView.layer.borderWidth = 1.0f;
    textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    // Do any additional setup after loading the view.
    
    NSLog(@"item detail: %@", itemDetail);
    
    NSString *imgUrlStr = [itemDetail objectForKey:@"images"][0];
    //[self loadImageAsyncWithURL:imgUrlStr forImageView:cell.itemImageView];
    //[cell.itemImageView setImageWithURL:imgUrlStr];

    NSURL *imageUrl = [NSURL URLWithString:imgUrlStr];
    [self.itemImageView setImageWithURL:imageUrl];
    NSString *body = itemDetail[@"body"];
    self.descriptionLabel.text = body;
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    
    //For "Vijay @Apple Dev"
    //NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"@(\\w+)" options:0 error:&error];
    
    NSArray *matches = [regex matchesInString:body options:0 range:NSMakeRange(0, body.length)];
    NSLog(@"matches: %@", matches);
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:body];
    
    NSInteger stringLength=[body length];
    
    for (NSTextCheckingResult *match in matches) {
        
        NSRange wordRange = [match rangeAtIndex:1];
        
        NSString* word = [body substringWithRange:wordRange];
        
        //Set Font
        UIFont *font=[UIFont fontWithName:@"Helvetica-Bold" size:15.0f];
        [attString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, stringLength)];
        
        
        //Set Background Color
        UIColor *backgroundColor=[UIColor orangeColor];
        [attString addAttribute:NSBackgroundColorAttributeName value:backgroundColor range:wordRange];
        
        //Set Foreground Color
        UIColor *foregroundColor=[UIColor blueColor];
        [attString addAttribute:NSForegroundColorAttributeName value:foregroundColor range:wordRange];
        
        NSLog(@"Found tag %@", word);
        
    }
    //self.tagsView
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
