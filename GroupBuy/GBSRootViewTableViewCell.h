//
//  GBSRootViewTableViewCell.h
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-10-11.
//  Copyright © 2015 Wenfeng Su. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GBSRootViewTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIImageView *itemImageView;
@property (nonatomic, weak) IBOutlet UILabel *itemBodyLabel;
@property (nonatomic, weak) IBOutlet UILabel *itemTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *itemPostDateLabel;

@end
