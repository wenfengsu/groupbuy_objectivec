//
//  GBAccountManager.h
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-06.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AppDelegate.h"


@protocol GBAccountLoginDelegate <NSObject>

@required
-(void) accountManager:(id)manager didSuccessfullyLogin:(id)options;
-(void) accountManager:(id)manager didFailedLogin:(id)options;


@end

enum{
    FACEBOOK,
    TWITTER,
    GOOGLE,
    GroupBuy
};

typedef NSInteger GBAccountType;

@interface GBAccountManager : NSObject{
    GBAccountType *accountType;
    AppDelegate *appDelegate;
    //id <GBAccountLoginDelegate> accountLogindelegate;
}

@property (nonatomic,weak) id accountLogindelegate;


-(void) facebookLoginClicked;
-(void) signupAndLoginUserWithGroupBuy;

@end
