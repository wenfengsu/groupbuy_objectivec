//
//  ViewController.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-06.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "GBAccountManager.h"
#import "GBInitialViewController.h"

@interface ViewController ()

@end

@implementation ViewController

//@synthesize logoutButton, facebookLoginButton, googleLoginButton, twiterLoginButton, gotoLogin;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*account = [GBAccountManager new];
    account.accountLogindelegate = self;
    
    self.logoutButton=[UIButton buttonWithType:UIButtonTypeCustom];
    self.logoutButton.backgroundColor=[UIColor darkGrayColor];
    self.logoutButton.frame=CGRectMake(0,0,180,40);
    self.logoutButton.center = self.view.center;
    self.logoutButton.hidden = YES;
    [self.logoutButton setTitle: @"Logout" forState: UIControlStateNormal];
    [self.logoutButton addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutButton];

    self.facebookLoginButton=[UIButton buttonWithType:UIButtonTypeCustom];
    self.facebookLoginButton.backgroundColor=[UIColor darkGrayColor];
    self.facebookLoginButton.frame=CGRectMake(0,0,180,40);
    self.facebookLoginButton.center = self.view.center;
    self.facebookLoginButton.hidden = YES;
    [self.facebookLoginButton setTitle: @"Facebook Login" forState: UIControlStateNormal];
    [self.facebookLoginButton addTarget:self action:@selector(facebookLoginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:facebookLoginButton];

    self.googleLoginButton=[UIButton buttonWithType:UIButtonTypeCustom];
    self.googleLoginButton.backgroundColor=[UIColor darkGrayColor];
    self.googleLoginButton.frame=CGRectMake(0,0,180,40);
    self.googleLoginButton.center = self.view.center;
    self.googleLoginButton.hidden = YES;
    [self.googleLoginButton setTitle: @"Google Login" forState: UIControlStateNormal];
    [self.googleLoginButton addTarget:self action:@selector(googleLoginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:googleLoginButton];
    
    self.twiterLoginButton=[UIButton buttonWithType:UIButtonTypeCustom];
    self.twiterLoginButton.backgroundColor=[UIColor darkGrayColor];
    self.twiterLoginButton.frame=CGRectMake(0,0,180,40);
    self.twiterLoginButton.center = self.view.center;
    self.twiterLoginButton.hidden = YES;
    [self.twiterLoginButton setTitle: @"Twitter Login" forState: UIControlStateNormal];
    [self.twiterLoginButton addTarget:self action:@selector(twitterLoginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:twiterLoginButton];
    
    self.gotoLogin=[UIButton buttonWithType:UIButtonTypeCustom];
    self.gotoLogin.backgroundColor=[UIColor darkGrayColor];
    self.gotoLogin.frame=CGRectMake(20,30,180,40);
    
    
    [self.gotoLogin setTitle: @"Go To" forState: UIControlStateNormal];
    [self.gotoLogin addTarget:self action:@selector(gotoLoginClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:gotoLogin];
    */
    
    
    // Do any additional setup after loading the view, typically from a nib.
    //FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    //loginButton.readPermissions =
    //@[@"public_profile", @"email", @"user_friends"];
    
    //[self.view addSubview:loginButton];
    
    /*[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_accessTokenChanged:)
                                                 name:FBSDKAccessTokenDidChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_currentProfileChanged:)
                                                 name:FBSDKProfileDidChangeNotification
                                               object:nil];
     */
    // It's generally important to check [FBSDKAccessToken currentAccessToken] at
    //  viewDidLoad to see if there is a token cached by the SDK or, resuming
    //  a login flow after eviction.
    // In this app, we want to see if there's a match with the local cache, and if
    //  not, clear the "current" user and token because that indicates either the
    //  SUCache version is incompatible.
    
    
    //[self updateContent:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    //self.loginButton = nil;
    // Dispose of any resources that can be recreated.
}

/*

// Observe a new token, so save it to our SUCache and update
// the cell.
- (void)_accessTokenChanged:(NSNotification *)notification
{
    FBSDKAccessToken *token = notification.userInfo[FBSDKAccessTokenChangeNewKey];
    NSLog(@"notification: %@", notification.userInfo);
    NSLog(@"token.userID: %@", token.userID);
    if (token) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user2222:%@", result);
             }
         }];
    }
}

// The profile information has changed, update the cell and cache.
- (void)_currentProfileChanged:(NSNotification *)notification
{
    //NSInteger slot = [self _userSlotFromIndexPath:_currentIndexPath];
    
    FBSDKProfile *profile = notification.userInfo[FBSDKProfileChangeNewKey];
    NSLog(@"token found44444: %@", profile);
    if (profile) {
        SUCacheItem *cacheItem = [SUCache itemForSlot:slot];
        cacheItem.profile = profile;
        [SUCache saveItem:cacheItem slot:slot];
        
        SUProfileTableViewCell *cell = (SUProfileTableViewCell *)[self.tableView cellForRowAtIndexPath:_currentIndexPath];
        cell.userName = cacheItem.profile.name;
    }
}*/


/*
-(void) getUserData{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"picture, email, name"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSLog(@"fetched user11111:%@", result);
             NSString *email = result[@"email"];
             NSString *social_id = result[@"id"];
             NSString *name = result[@"name"];
             // create the user in server and log him in
             
         }
     }];
}*/



-(void)gotoLoginClicked{
    NSLog(@"login");
    UIStoryboard *loginOptionsStoryBoard = [UIStoryboard storyboardWithName:@"login" bundle:nil];
    UIViewController *initialViewController = [loginOptionsStoryBoard instantiateInitialViewController];
    [self.navigationController pushViewController:initialViewController animated:YES];
    
    
}

-(void)logoutButtonClicked{
    NSLog(@"logout");
    [[FBSDKLoginManager new] logOut];
    [self updateContent:nil];
}

// Once the button is clicked, show the login dialog
-(void)facebookLoginButtonClicked
{
    //[account facebookLoginClicked];
}

-(void)googleLoginButtonClicked{
    
}

-(void)twitterLoginButtonClicked{
    
}

-(void)groupbuyLoginButtonClicked{
    
}



-(void) updateContent:(id) options{
    
   /* if (!options) {
        NSLog(@"show all login options");
        
    }
    
    
    if (options[@"email"]) {
        
        NSLog(@"should push to another view with logged in user %@", options);
        NSString *email = options[@"email"];
        NSString *socialId = options[@"id"];
        NSString *name = options[@"name"];
        NSString *avatar = options[@"picture"][@"data"][@"url"];
        NSString *loginType = @"FB";
        
        
        NSLog(@"%@, %@, %@, %@", email  , socialId, name, avatar);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        //dev IP: 192.168.1.6:9000
        //prod IP: 198.58.109.220
        NSDictionary *parameters = @{@"email": email, @"username": name, @"social_id": socialId, @"login_type": loginType};
        [manager POST:@"http://192.168.1.6:9000/api/v1.0/users/" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
        
    }*/
    
}

#pragma mark -Account manager delegate update view after login
- (void)updateLoginView:(id)options{
    //[self updateContent:options];
}


@end
