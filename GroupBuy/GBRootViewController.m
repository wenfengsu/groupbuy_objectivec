//
//  GBRootViewController.m
//  GroupBuy
//
//  Created by Wenfeng Su on 2015-09-18.
//  Copyright (c) 2015 Wenfeng Su. All rights reserved.
//

#import "GBRootViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "GBSRootViewTableViewCell.h"
#import "GBSListDetailViewController.h"
#import "GBSSessionManager.h"

#import "UIImageView+GBGroupBuy.h"
#import "GBSRootViewTableViewCell.h"


#define CENTER_TAG 1

@interface GBRootViewController ()

@end

@implementation GBRootViewController
@synthesize listingTableView, buyingButton, sellingButton, listings, nextURL;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    listings = [NSMutableArray new];
    
    /*GBHttpManager *httpManager = [GBHttpManager httpManager];
    httpManager.delegate = self;
    
    
    [httpManager getData:@"posts" requestType:GBRequestTypeGet parameters:nil];
    */
    
    
    //[[GBSDataRequest sharedRequest] GET:@"test" parameters:nil success:nil failure:nil];
    NSString *url = @"http://198.58.109.220/api/v1.0/posts/";
    
    __weak typeof (self) weakSelf = self;
    [[GBSSessionManager sharedSessionManager] GET:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"got result: %@", responseObject);
        
        __weak typeof (weakSelf) strongSelf = weakSelf;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (responseObject[@"posts"]){
                
                
                [strongSelf.listings addObjectsFromArray:responseObject[@"posts"]];
                [strongSelf.listingTableView reloadData];
            }
        });
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error rootview");
    }];
    
    
    
    
    
    
    
    
    
    
    // Do any additional setup after loading the view.
}

/*
#pragma mark - GBHttpRequestManagerDelegate
-(void) httpManager:(id)manager didFinishLoadingWithResult:(NSDictionary*) results{
    
    
    NSLog(@"root view controller result : %@", results);
    //self.nextURL = [results objectForKey:@"next"];
    //self.listings = [results objectForKey:@"posts"];
    if (results[@"posts"]){
        [listings addObjectsFromArray:results[@"posts"]];
        [listingTableView reloadData];
    }
    
    
}

-(void) httpManager:(id)manager didFinishWithError:(NSDictionary *) results{
    NSLog(@"rootiewcontroller error: %@", results);
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return [listings count];
}


- (GBSRootViewTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"GBSRootViewTableViewCell";
    GBSRootViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
    if (cell == nil) {
        cell = [[GBSRootViewTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *post = [listings objectAtIndex:indexPath.row];
    //NSLog(@"post: %@", post);
    //cell.itemBodyLabel.text = [[listings objectAtIndex:indexPath.row] objectForKey:@"body"];
    //cell.itemPostDateLabel.text = [[listings objectAtIndex:indexPath.row] objectForKey:@"timestamp"];
    
    
    /*cell.itemPostDateLabel.text = [NSString stringWithFormat:@"%@ by %@", [[listings objectAtIndex:indexPath.row] objectForKey:@"timestamp"], [[[listings objectAtIndex:indexPath.row] objectForKey:@"seller"] objectForKey:@"name"]];
    
    
    
     
    NSString *imgUrlStr = [[listings objectAtIndex:indexPath.row] objectForKey:@"images"][0];
    //[self loadImageAsyncWithURL:imgUrlStr forImageView:cell.itemImageView];
    [cell.itemImageView setImageWithURL:imgUrlStr];
    
    
    NSString *picCount = [NSString stringWithFormat:@"%lu", [[[listings objectAtIndex:indexPath.row] objectForKey:@"images"] count]];
    
    cell.numberOfPicsLabel.text = picCount;
    NSString *buyer_count = [NSString stringWithFormat:@"%@ wants", [[listings objectAtIndex:indexPath.row] objectForKey:@"buyers_count"]];
    NSString *comment_count = [NSString stringWithFormat:@"%@ comments", [[listings objectAtIndex:indexPath.row] objectForKey:@"comment_count"]];
    
    cell.numberOfWantLabel.text = buyer_count;
    cell.numberOfCommentsLabel.text = comment_count;
    */
    if ([post[@"images"] count] > 0) {
        NSString *imgUrlStr = [[listings objectAtIndex:indexPath.row] objectForKey:@"images"][0];
        NSURL *imageUrl = [NSURL URLWithString:imgUrlStr];
        [cell.itemImageView setImageWithURL:imageUrl];
        
    }else{
        cell.itemImageView.image = [UIImage imageNamed:@"ph.jpg"];
    }
    
    cell.itemTitleLabel.text =  post[@"title"];
    cell.itemBodyLabel.text = post[@"body"];
    cell.itemPostDateLabel.text = post[@"timestamp"];
    return cell;
 

 }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"did select row");
    
    //[self prepareForSegue:<#(nonnull UIStoryboardSegue *)#> sender:<#(nullable id)#>]
    //UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    //[self performSegueWithIdentifier:@"SegueIdentifer" sender:cell];
}



 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     NSLog(@"segue before navigation: %@", [segue identifier]);
     if ([[segue identifier] isEqualToString:@"itemDetail"])
     {
         NSLog(@"assign value to the destination viewcontroller");
         
         NSIndexPath *indexPath = [self.listingTableView indexPathForSelectedRow];
         NSDictionary *detail = self.listings[indexPath.row];
         GBSListDetailViewController *detailViewController = segue.destinationViewController;
         detailViewController.itemDetail = detail;
         
         
         // Get reference to the destination view controller
         //GBSListDetailViewController *listDetailView = [segue destinationViewController];
         
         // Pass any objects to the view controller here, like...
         //[vc setMyObjectHere:object];
     }
 }



-(IBAction)wantButtonClicked:(id)sender{
    NSLog(@"want this item");
}

-(IBAction)commentButtonClicked:(id)sender{
    NSLog(@"comment this item");
}

-(IBAction)menuButtonClicked:(id)sender{
    NSLog(@"menu buton clicked");
    //[self performSegueWithIdentifier: @"SongSegue" sender: self];
   
}

-(IBAction)buyButtonClicked:(id)sender{
    NSLog(@"buy buton clicked");
}

-(IBAction)sellButtonClicked:(id)sender{
    NSLog(@"sell buton clicked");
}

@end
